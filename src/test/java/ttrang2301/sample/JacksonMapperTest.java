package ttrang2301.sample;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonMapperTest {

	@Test
	public void testSerializer_whenObjectSerialized_thenTypeValueIsStringAndEqualsToDisplayedText() throws Exception {
		CDFDefinitionCreateRequest obj = new CDFDefinitionCreateRequest(1, "Name", Type.TEXT, CategoryCode.UDF);

		ObjectMapper m = new ObjectMapper();

		String json = m.writeValueAsString(obj);
		assertThat(json, containsString("\"" + Type.TEXT.getDisplayedText() + "\""));
	}

	@Test
	public void testDeserializer_whenObjectDeserialized_thenTypeValueIsEqualsToDisplayedText() throws Exception {
		CDFDefinitionCreateRequest obj = new CDFDefinitionCreateRequest(1, "Name", Type.TEXT, CategoryCode.UDF);

		ObjectMapper m = new ObjectMapper();

		String json = "{\"type\": \"Text\"}";

		CDFDefinitionCreateRequest readValue = m.readValue(json, CDFDefinitionCreateRequest.class);
		assertThat(readValue.type, equalTo(obj.type));
	}

}
