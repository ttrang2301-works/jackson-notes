package ttrang2301.sample;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CDFDefinitionCreateRequest {

	public int id;
	public String name;
	@JsonSerialize(converter = Type.Serializer.class)
	@JsonDeserialize(converter = Type.Deserializer.class)
	public Type type;
	public CategoryCode categoryCode;

	public CDFDefinitionCreateRequest() {
	}

	public CDFDefinitionCreateRequest(int id, String name, Type type, CategoryCode categoryCode) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.categoryCode = categoryCode;
	}
}
