package ttrang2301.sample;

import java.util.Objects;

import com.fasterxml.jackson.databind.util.StdConverter;

public enum Type {

	TEXT("Text"), DATE("Date"), DROPDOWN("Dropdown");

	private String displayedText;

	Type(String displayedText) {
		this.displayedText = displayedText;
	}

	public String getDisplayedText() {
		return displayedText;
	}

	public static final class Deserializer extends StdConverter<String, Type> {

		@Override
		public Type convert(String value) {
			if (value == null) {
				return null;
			}
			for (Type type : Type.values()) {
				if (Objects.equals(type.getDisplayedText(), value)) {
					return type;
				}
			}
			throw new RuntimeException("Unsupported type: '" + value + "'");
		}

	}

	public static final class Serializer extends StdConverter<Type, String> {

		@Override
		public String convert(Type type) {
			if (type == null) {
				return null;
			}
			return type.getDisplayedText();
		}

	}
}
