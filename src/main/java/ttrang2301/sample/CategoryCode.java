package ttrang2301.sample;

public enum CategoryCode {

	UDF("UDF");

	private String displayedText;

	CategoryCode(String displayedText) {
		this.displayedText = displayedText;
	}

	public String getDisplayedText() {
		return displayedText;
	}
}
